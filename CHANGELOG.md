# Changelog

## [2.8.1](https://gitlab.developers.cam.ac.uk/uis/devops/tools/logan/compare/2.8.0...2.8.1) (2024-12-09)

### Bug Fixes

* **deps:** update all non-major dependencies ([f59eff4](https://gitlab.developers.cam.ac.uk/uis/devops/tools/logan/commit/f59eff427252ba82458f41f5d4726c469b822c0f))
* **deps:** update all non-major dependencies ([3180b1f](https://gitlab.developers.cam.ac.uk/uis/devops/tools/logan/commit/3180b1f73e00079d1b2d4b3757aba4b389549f5e))
* **deps:** update all non-major dependencies ([6f88982](https://gitlab.developers.cam.ac.uk/uis/devops/tools/logan/commit/6f889827f1afc6e4fabeda01188949c0fc163205))
* **deps:** update dependency faker to v30.3.0 ([a1ad34b](https://gitlab.developers.cam.ac.uk/uis/devops/tools/logan/commit/a1ad34b8e34b0311370dee2f5015d313e115624f))
* **deps:** update dependency faker to v30.4.0 ([edd60a0](https://gitlab.developers.cam.ac.uk/uis/devops/tools/logan/commit/edd60a00aafe4fdf3e1f56a963911307dd267929))
* **deps:** update dependency faker to v30.8.0 ([cff6d85](https://gitlab.developers.cam.ac.uk/uis/devops/tools/logan/commit/cff6d85fa64e8c85d0abde3594fe807a093bcefc))
* **deps:** update dependency faker to v32 ([19b4def](https://gitlab.developers.cam.ac.uk/uis/devops/tools/logan/commit/19b4defebba0bce24072859e9813e9fec3ba73e8))
* **deps:** update dependency faker to v33 ([bab1011](https://gitlab.developers.cam.ac.uk/uis/devops/tools/logan/commit/bab1011fc5e81c8835e5b0898c26dc319dafa357))
* **deps:** update dependency faker to v33.1.0 ([0d16a4c](https://gitlab.developers.cam.ac.uk/uis/devops/tools/logan/commit/0d16a4c8d80f67dbab568a5f5b386f60d29c8666))
* **deps:** update dependency google-auth to v2.36.0 ([b7d7aff](https://gitlab.developers.cam.ac.uk/uis/devops/tools/logan/commit/b7d7affff2fdd8162b735a86cf0e71651c33579e))
* **deps:** update dependency pydantic to v2.10.0 ([0fafded](https://gitlab.developers.cam.ac.uk/uis/devops/tools/logan/commit/0fafdedb13ce5ca0c4958ec291b13990b3b3d9e9))
* **deps:** update dependency pydantic to v2.10.2 ([3ef3225](https://gitlab.developers.cam.ac.uk/uis/devops/tools/logan/commit/3ef32252adaa1adebc35995b4388d558fb888b09))
* **deps:** update dependency pydantic to v2.10.3 ([1cb3823](https://gitlab.developers.cam.ac.uk/uis/devops/tools/logan/commit/1cb3823618d98d2209add2fa26aecfe3541e90a5))
* **deps:** update dependency pydantic-settings to v2.6.0 ([664d9d3](https://gitlab.developers.cam.ac.uk/uis/devops/tools/logan/commit/664d9d33f66279abf8d93c9cc425cf3b48779def))
* **deps:** update dependency rich to v13.9.3 ([e546bf6](https://gitlab.developers.cam.ac.uk/uis/devops/tools/logan/commit/e546bf63b2c5d01c5ea89ae07d76c52f2bef877a))
* **pre-commit:** fix spurious flake8 warnings ([5d3e384](https://gitlab.developers.cam.ac.uk/uis/devops/tools/logan/commit/5d3e384fad235bde8a4e9ea35f332ea13ea69e1b))

## [2.8.0](https://gitlab.developers.cam.ac.uk/uis/devops/tools/logan/compare/2.7.0...2.8.0) (2024-10-07)


### Features

* bump minimum python version ([410e7a7](https://gitlab.developers.cam.ac.uk/uis/devops/tools/logan/commit/410e7a776988f83c2709fe8ce8be5be7d1609697))
* **renovate:** use poetry grouping preset ([cdcd711](https://gitlab.developers.cam.ac.uk/uis/devops/tools/logan/commit/cdcd711e56c2351f05f0ba548ac90dbf4e76ae2b))


### Bug Fixes

* **deps:** update major poetry dependencies ([1126d79](https://gitlab.developers.cam.ac.uk/uis/devops/tools/logan/commit/1126d79d5e151613b709410df44451aec4098a86))
* exempt auto-generated CHANGELOG from prettier modification ([8744263](https://gitlab.developers.cam.ac.uk/uis/devops/tools/logan/commit/874426335ca6185358f2b1e775c361d87968d72c))

## [2.7.0](https://gitlab.developers.cam.ac.uk/uis/devops/tools/logan/compare/2.6.0...2.7.0) (2024-10-03)

### Features

- add release-it to CI ([a640f1f](https://gitlab.developers.cam.ac.uk/uis/devops/tools/logan/commit/a640f1f53e0e4de2b20e05715600a27cc01ae3cb))

### Bug Fixes

- **deps:** update dependency faker to v30 ([504e9b6](https://gitlab.developers.cam.ac.uk/uis/devops/tools/logan/commit/504e9b6b8c4bd695d937d1e08740fae148903aaa))
- **deps:** update dependency google-cloud-kms to v3 ([dc87365](https://gitlab.developers.cam.ac.uk/uis/devops/tools/logan/commit/dc87365ceb564d9d00a043b5798b2dc0d595f833))
- **deps:** update dependency structlog to v24 ([3193b8c](https://gitlab.developers.cam.ac.uk/uis/devops/tools/logan/commit/3193b8c6a711fa997d727dfac827bb8a4135cdc6))

#

### 2.6.0

- feat(sanctuary): sanctuary now can synchronise/destroy old versions for selected secret(s).
  Option `-s, NAME, --secret NAME` added for this.

### 2.5.3

- fix(sanctuary): in a `--dry-run` mode the tool now checks if secret exists on GCP side.
  Also, the `--dry-run` mode is now always verbose.

### 2.5.2

- fix(sanctuary): command line option `-c FILE, --configuration FILE` now behaves similarly
  to the one in `logan`

### 2.5.1

- Move away from `pkg_resources` which is removed in later Pythons towards `importlib`.

### 2.5.0

- Add explicit rather than implicit secret destruction via `sanctuary destroy-old-versions`.

### 2.4.0

- Add factory mode support

### 2.3.1

- fix(sanctuary): Fix function \_destroy_all_but_latest_version. Changes related to
  a detection mechanism of secret's version status.

### 2.3.0

- feat(sanctuary): add use_field_labels option

### 2.2.0

- feat(sanctuary): better error reporting for 1password items
- fix(sanctuary): update docs to make policy for Google Secrets clear

### 2.1.1

- Only destroy non-destroyed Google secret versions when updating secret versions.

### 2.1.0

- Add new "sanctuary" tool. See doc/sanctuary.md.

### 2.0.1

- Reintroduce support for KMS keys.

### 2.0.0

- Move to using poetry for packaging.
- Remove support for KMS keys.

### 1.1.4

- Add `-v` `--version` show version feature

### 1.1.3

- Add support for GCP service account impersonation.

### 1.1.2

- Replace the OS X requirement for pinata-sshd with a direct mount point for the
  host's ssh-agent socket.

### 1.1.1

- Reflect exit code of wrapped process in logan's exit code rather than showing
  a Python stack trace on a non-zero exit.

### 1.1.0

- Add support for `mount_docker_socket` option in configuration.
- Release version 1.1 of the configuration format.
- Allow specifying minimum required version via `version` option in
  configuration.
