# Copying secrets via sanctuary

We occasionally have to "bootstrap" a new product with secrets which come from
"outside" and cannot be created or managed via terraform. For example, if we
need to have an associated Lookup group for a product, the credentials for that
group need to be created manually.

We use 1password as the canonical location for such secrets but we have a
problem when it comes to using those secrets in a terraform configuration. We
usually run terraform configurations by impersonating a designated "deployment"
service account which allows us to run terraform deployments in CI by granting
the CI service account the ability to impersonate the deployment service
account. Unfortunately it is not so simple to give our CI jobs access to the
1password secrets.

> 1password's automation features may help with this but we do not have that
> feature available yet.

Historically we've solved this by having a "bootstrap" terraform configuration
copy 1password secrets into Google Cloud secrets which the main terraform
deployment can read. These root bootstrap configurations are fairly unwieldy
and come with a lot of unnecessary copy-pasting but a stronger motivation for
moving away from this model is that the secrets end up in plaintext stored in
terraform state files. Moving to a model where secrets live only in 1password
and Google Secrets is an increase in security.

Instead logan includes a "sanctuary" tool which can copy secrets from 1password
into Google Cloud. These secrets can be initialised or updated by a developer
who is a) sufficiently privileged and b) has the [1password CLI
tool](https://1password.com/downloads/command-line/) installed.

## Getting started

Make sure that you have the [1password CLI tool
installed](https://1password.com/downloads/command-line/). You should be able,
for example, to list your vaults via the `op vault list command`.

The sanctuary tool should have been installed as part of `logan`. Check that
you can run it via the `sanctuary --help` command.

Sanctuary uses the same configuration file as logan but uses a special
`sanctuary` section within it.

## Usage

See following output of `$ sanctuary --help`:

```console
Synchronise secrets when bootstapping projects.

Usage:
    sanctuary (-h | --help)
    sanctuary sync [--secret NAME...] [--configuration FILE] [--verbose] [--dry-run]
    sanctuary destroy-old-versions [--secret NAME...] [--configuration FILE] [--verbose] [--dry-run]

Options:

    -h, --help                          Show a brief usage summary.
    -c FILE, --configuration FILE       Location of configuration file.
    --verbose                           Be verbose when logging.
    --dry-run                           Don't make changes but log what would be done.
                                        Always verbose.
    -s, NAME, --secret NAME             Work with provided secret(s) only.
```

## A small example

Suppose one has a login credential in 1password with the id `abcdefghijklmnop`
which you want to synchronise with a secret named "super-secret-credential" in
a Google project named "our-great-project". You could use the following
configuration:

```yaml
# .logan.yaml
version: "1.3"

sanctuary:
  secrets:
    some-name:
      from:
        op-cli-item:
          item-id: abcdefghijklmnop
          fields: [username, password]
          use_field_labels: true
      to:
        google-secret:
          project: our-great-project
          name: super-secret-credential
```

### Synchronising secrets

Running `sanctuary sync` will synchronise the secret from 1password into the
Google secret. The `some-name` key is arbitrary and is only used to name the
secret within the sanctuary configuration.
By default the tool will synchronise all secrets from the `sanctuary.secrets` list.
If it is needed to synchronise only a selected secret(s), option `-s, --secret` can be used as shown:

```sh
sanctuary sync --secret some-name1 --secret some-name2 # or -s some-name1 -s some-name2
```

The `use_field_labels` option for `op-cli-item` means that fields are retrieved by the human-visible
labels rather than the auto-generated ids. For items created manually in the 1password GUI, this may
be a more convenient option to use. Note that field _labels_ can be changed but field _ids_ are
immutable. As such it is slightly safer to use ids but more convenient to use labels.

The internal field ids can be seen by dumping a 1password item via `op item get {itemid} --format=json`.

The Google secret will be a JSON formatted string of the following form:

```json
{
  "username": "...",
  "password": "..."
}
```

**Previous versions of the Google secret will not be destroyed.** If you
wish for that to happen add `destroy_previous_versions: true` to the Google
secret configuration.

### Destroying previous versions

As noted above, sanctuary defaults to not destroying old versions of secrets. This is to allow you
to safely specify a specific version number in your application's configuration. If you are
confident that your application is using the most recent version of all secrets, you can destroy all
older versions via `sanctuary destroy_old_versions`.
By default the tool will destroying old versions for all secrets from the `sanctuary.secrets` list.
If it is needed to destroy old versions only for a selected secret(s), option `-s, --secret`
can be used as shown:

```sh
sanctuary destroy-old-versions --secret some-name1 --secret some-name2 # or -s some-name1 -s some-name2
```

## Performing a dry run

The `--dry-run` option to `sanctuary sync` and `sanctuary destroy-old-versions` will print out what
the tool _would_ do but does not actually make any changes.

## Using 1password documents

Documents can be used by specifying the "from" source via `op-cli-document`.
The document id can be any id returned from `op document list`. For example:

```yaml
# .logan.yaml
sanctuary:
  secrets:
    some-name:
      from:
        op-cli-document:
          document-id: abcdefghijklmnop
      to:
        google-secret:
          project: our-great-project
          name: super-secret-credential
```

In this case the Google secret will consist of the document's content.

## Using raw field values

Sometimes you just want the content of a single field in a secret without any
additional JSON formatting. This can be done by specifying the `field` option
to `op-cli-item`:

```yaml
# .logan.yaml
sanctuary:
  secrets:
    some-name:
      from:
        op-cli-item:
          item-id: abcdefghijklmnop
          field: "api-key"
      to:
        google-secret:
          project: our-great-project
          name: super-secret-credential
```

In this case the Google secret will consist of the field's value with no
additional formatting.

## Creating Google Secret Manager Secrets

By design sanctuary **does not create Google Secret Manager Secrets**. The creation of secrets and
associated IAM permissions are assumed to be managed by other tools such as terraform. **Sanctuary
will create new Secret Versions but not the Secrets themselves**.

Since sanctuary can manage the secret value versions (create, destroy), please do not manage
resources of type `google_secret_manager_secret_version` via terraform. In some cases, it might
cause the terrafrom state inconsistency and/or unexpected value of the latest secret version.

The following terraform code will create a secret with no secret value. Sanctuary
can then be used to update the secret value without terraform attempting to update the version.

```tf
# A secret which stores some credential.
resource "google_secret_manager_secret" "my_credential" {
  secret_id = "my-credential"
}
```

Alternatively, the secret can be created with a [gcp-secret-manager](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-secret-manager) terraform module
with option `manage_secret_versions` set to `false`.

```tf
module "my_credential" {
  source  = "gitlab.developers.cam.ac.uk/uis/gcp-secret-manager/devops"
  version = "<5.0.0"

  project                = "some-project-id"
  region                 = "europe-west2"
  secret_id              = "my-credential"
  manage_secret_versions = false
}
```

## Using Google service account impersonation

Google secrets will be updated using default application credentials. If you want to instead use
impersonated service account credentials, add a `google-secret` section to the sanctuary
configuration:

```yaml
# .logan.yaml
sanctuary:
  google-secret:
    impersonate: some-service-account@example-project.iam.gserviceaccount.com
  secrets:
    # ... etc
```

## Keeping your configuration DRY using anchors

YAML anchors can be used to use a common Google project all secrets:

```yaml
# .logan.yaml
sanctuary:
  secrets:
    first-secret:
      from:
        op-cli-item:
          item-id: abcdefghijklmnop
          fields: [username, password]
      to:
        google-secret:
          project: &google-project our-great-project
          name: super-secret-credential
    second-secret:
      from:
        op-cli-document:
          document-id: qrstuvwxyz123456
      to:
        google-secret:
          project: *google-project
          name: some-secret-document
```

## Accessing the secret value from terraform code

In some cases, it is needed to retrieve the secret value from GCP and use it in terraform.
But as stated above, it is recommended not to manage secret versions with terraform.
The example below shows how to retrieve the latest secret version using a `data` block.

```tf
data "google_secret_manager_secret" "my_credential" {
  secret_id = "my-credential"
}

data "google_secret_manager_secret_version" "latest" {
  secret  = data.google_secret_manager_secret.my_credential.name
  version = "latest"
}
```

After this, the secret value can be used by referencing the object
`data.google_secret_manager_secret_version.latest.secret_data`.

Please note, that using this approach, the terraform will store the secret value
in the state file as a plain text.
