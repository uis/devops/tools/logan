"""
Parse and load configuration.

"""

import logging
import os
import typing

import yaml
from schema import And, Optional, Or, Schema, SchemaError

LOG = logging.getLogger(__name__)

CONFIGURATIONSCHEMA = {
    Optional("version", default="1.0"): str,
    "project": And(str, len),
    Optional("secrets", default=[]): [
        {
            Optional("name", default="secret"): And(str, len),
            "source": And(str, len),
            "target": And(str, len),
            Optional("key", default={}): {str: object},
        }
    ],
    Optional("image", default=""): str,
    Optional("build"): {
        Optional("dockerfile"): str,
        Optional("context"): str,
    },
    Optional("env", default={}): {str: Or(str, None)},
    Optional("mount_docker_socket"): Or(None, bool),
    Optional("use_impersonation"): Or(None, bool),
    Optional("factory_mode"): Or(None, bool),
    Optional("factory_vars_path", default="./product-vars"): str,
    Optional("factory_prefix", default=""): str,
    # Sanctuary configuration is handled within logan.sanctuary.
    Optional("sanctuary", default={}): {object: object},
}


class ConfigurationError(RuntimeError):
    """
    Base class for all errors in loading configuration.

    """


class ConfigurationParseError(ConfigurationError):
    """
    Indicates a problem with the format of the configuration file.

    """


class ConfigurationNotFound(ConfigurationError):
    """
    Indicates that the configuration file could not be found in the location search path.

    """

    def __init__(self):
        return super().__init__("Could not find configuration file")


def find_configuration(location: typing.Optional[str] = None) -> str:
    """
    Return the location of the configuration file. If *location* is passed, it is used as the first
    search location.

    :raises: ConfigurationNotFound if the configuration file does not exist.
    """
    if location is not None:
        path = location
    else:
        path = os.path.join(os.getcwd(), ".logan.yaml")

    if not os.path.isfile(path):
        LOG.error('Could not find configuration file "%s"', path)
        raise ConfigurationNotFound()

    return path


def load_configuration(location=None):
    """
    Load configuration and return a dictionary of settings.
    Pass a non-None location to override the default search path.

    :raises: ConfigurationError if the configuration could not be loaded.

    """
    path = find_configuration(location)

    with open(path) as fobj:
        return _load_config(fobj)


def _load_config(fobj_or_string):
    """
    Load configuration from either a file-like object or a string.
    Validate the configuration against schema.

    Returns a dictionary of configuration options

    >>> _load_config("notproject: my-project")
    Traceback (most recent call last):
        ...
    logan.config.ConfigurationParseError: Configuration does not match schema

    >>> def assert_is_subset(adict, bdict):
    ...     assert adict.items() >= bdict.items()

    >>> c = _load_config("project: my-project")
    >>> assert_is_subset(c, {
    ...     'project': 'my-project', 'secrets': [], 'image': '', 'env': {},
    ... })

    >>> c = _load_config('''
    ... project: my-project
    ... image: uisautomation/logan-terraform
    ... env:
    ...   TERRAFORM_KUBECONFIG_OUTPUT: some_terraform_output''')
    >>> assert_is_subset(c, {
    ...     'project': 'my-project', 'secrets': [],
    ...     'image': 'uisautomation/logan-terraform',
    ...     'env': {'TERRAFORM_KUBECONFIG_OUTPUT': 'some_terraform_output'}
    ... })

    >>> c = _load_config('''
    ... project: my-project
    ... secrets:
    ...   - name: secret
    ...     source: secret.enc
    ...     target: /secret.json''')
    >>> assert_is_subset(c, {
    ...     'project': 'my-project', 'image': '', 'env': {},
    ...     'secrets': [
    ...          {'name': 'secret', 'source': 'secret.enc', 'target': '/secret.json', 'key': {}}
    ...      ]
    ... })

    >>> c = _load_config('''
    ... project: my-project
    ... env:
    ...     VARIABLE_WITH_VALUE: some_value
    ...     USE_SHELL_VARIABLE:''')
    >>> assert_is_subset(c, {
    ...     'project': 'my-project', 'secrets': [], 'image': '',
    ...     'env': {'VARIABLE_WITH_VALUE': 'some_value', 'USE_SHELL_VARIABLE': None}
    ... })

    >>> c = _load_config('''
    ... project: my-project
    ... build:
    ...   dockerfile: 'foo'
    ... ''')
    >>> assert_is_subset(c, {'build': {'dockerfile': 'foo'}})

    >>> c = _load_config('''
    ... project: my-project
    ... build:
    ...   context: 'foo'
    ... ''')
    >>> assert_is_subset(c, {'build': {'context': 'foo'}})

    >>> c = _load_config('''
    ... project: my-project
    ... sanctuary:
    ...   ignored_by_logan: true
    ... ''')

    """
    conf = yaml.safe_load(fobj_or_string)

    # validate schema
    try:
        return Schema(CONFIGURATIONSCHEMA).validate(conf)
    except SchemaError as e:
        LOG.error('Configuration file does not match schema "%s"', e)
        raise ConfigurationParseError("Configuration does not match schema")
