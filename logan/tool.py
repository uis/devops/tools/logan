"""
Utility which wraps "docker run" so that secrets, etc are auto-mounted
"""

import argparse
import importlib.metadata
import itertools
import logging
import os
import platform
import shlex
import shutil
import stat
import subprocess
import sys
import tempfile
import urllib.parse
import warnings

from google.cloud import kms, secretmanager_v1
from rich.logging import RichHandler

from . import config

LOG_FORMAT = "%(message)s"

# Create a logger for this script.
LOG = logging.getLogger(__name__)

# Search the executable path for the docker utility.
DOCKER = shutil.which("docker")

# Maximum supported configuration version
MAX_CONF_VERSION = (1, 4)

# Ignore warnings from Google API clients about using personal account credentials as application
# default credentials
warnings.filterwarnings("ignore", module="google.auth")


def main():
    """
    Application entrypoint.

    """
    # Parse command line arguments.
    parser = create_parser()
    args = parser.parse_args()

    # Configure logging
    logging.basicConfig(
        level=logging.WARN if args.quiet else logging.DEBUG if args.debug else logging.INFO,
        format=LOG_FORMAT,
        datefmt="[%d-%m-%Y %X]",
        handlers=[RichHandler(markup=True, show_time=False)],
    )

    if args.version:
        version = importlib.metadata.version("logan")
        LOG.info(f"Logan version: {version}")
        sys.exit(0)

    # Load configuration
    try:
        conf = config.load_configuration(location=args.configuration)
    except config.ConfigurationError as e:
        LOG.error("Could not load configuration: %s", e)
        sys.exit(1)

    # Check version number. Parse into tuple of ints to allow direct comparison.
    version = tuple(int(v) for v in conf["version"].split("."))
    if version > MAX_CONF_VERSION:
        LOG.error(
            "Configuration version %s is too new. Maximum supported is %s",
            conf["version"],
            ".".join(str(v) for v in MAX_CONF_VERSION),
        )
        sys.exit(1)

    if not conf.get("factory_mode") and (args.factory_exclude or args.factory_only):
        LOG.error(
            "This project is not set to use factory mode, enable `factory_mode` in .logan.yaml"
        )
        sys.exit(1)

    if conf.get("factory_mode") is not None:
        if version < (1, 4):
            LOG.error("factory_mode is only valid for configuration versions >= 1.4")
            sys.exit(1)

        if conf.get("factory_prefix") == "":
            conf["factory_prefix"] = conf["project"]

        LOG.info("[purple]Factory Config")
        factory_mode = conf["factory_mode"]

        if conf["factory_mode"]:
            conf_dir = os.path.dirname(config.find_configuration(args.configuration))
            prod_path = os.path.relpath(conf.get("factory_vars_path"), start=conf_dir)
            items = sorted(set(next(os.walk(prod_path))[1]))

            LOG.debug(f"[purple]Factory items {items}")

            if args.factory_only:
                LOG.debug(f"[purple]Only running for item {args.factory_only}")
                if not set(args.factory_only).issubset(items):
                    LOG.error("Requested item not found in vars dir")
                    sys.exit(1)
                items = sorted(set(args.factory_only))

            if args.factory_exclude:
                LOG.debug(f"[purple]Excluding items {args.factory_exclude}")
                items = set(items) - set(args.factory_exclude)

            items = sorted(items)

            if len(items) <= 0:
                LOG.error("No items left in run")
                sys.exit(1)
            LOG.info(f"[purple]Factory items in run {items}")
    else:
        factory_mode = False

    if conf.get("sanctuary", {}) != {} and version < (1, 3):
        LOG.error("Sanctuary requires version 1.3 configuration.")
        sys.exit(1)

    # replace {project} in args with configuration file's project option
    d = vars(args)
    for k, v in d.items():
        if isinstance(v, str):
            d[k] = v.replace("{project}", conf["project"])

    # If image specified then use of it instead of building from Dockerfile
    if conf["image"] != "":
        # Override configuration if specified by command line
        if args.image_name != "":
            LOG.info('Image to use/pull overrided with "%s"', args.image_name)
            conf["image"] = args.image_name
        else:
            args.image_name = conf["image"]
        # Pull image if not disabled. This is done on each invocation to ensure
        # that the image is up-to-date
        if not args.nopull:
            LOG.info('Pulling docker image "%s"', conf["image"])
            if not args.dry_run:
                run_docker("pull", conf["image"])
    else:
        # If no image-name specified on command line then default to project
        if args.image_name == "":
            args.image_name = conf["project"]
        # Build container if not disabled. This is done on each invocation to ensure
        # that the image is up-to-date w.r.t. the repository.
        if not args.nobuild:
            LOG.info('Building docker image "%s"', args.image_name)
            if not args.dry_run:
                build = conf.get("build", {})
                context_path = os.path.abspath(build.get("context", "."))
                dockerfile_path = os.path.abspath(build.get("dockerfile", "Dockerfile"))

                if os.path.commonpath([os.getcwd(), context_path]) != os.getcwd():
                    LOG.error("Context cannot be outside of current directory")
                    raise RuntimeError()

                if os.path.commonpath([os.getcwd(), dockerfile_path]) != os.getcwd():
                    LOG.error("Docker file cannot be outside of current directory")
                    raise RuntimeError()

                run_docker("build", "-f", dockerfile_path, "-t", args.image_name, context_path)

    # If asked, remove the existing terraform data volume.
    if args.rm_terraform_data_volume:
        if factory_mode:
            LOG.info("[purple]Factory run")
            for item in items:
                LOG.info(f"[purple]Item - {item}")
                item_volume = f"{conf.get('factory_prefix')}_{item}"
                LOG.info('Removing terraform data volume "%s"', item_volume)
                if not args.dry_run:
                    run_docker("volume", "rm", "-f", item_volume)
        else:
            LOG.info('Removing terraform data volume "%s"', args.terraform_data_volume_name)
            if not args.dry_run:
                run_docker("volume", "rm", "-f", args.terraform_data_volume_name)

    # HACK: Docker in OS X does not mount user-specific temporary directories by default.
    if platform.system() == "Darwin" and args.temp_directory is None:
        LOG.info('Using "/tmp" as default temporary directory on OS X.')
        args.temp_directory = "/tmp"

    # Do remaining work inside a temporary directory.
    if factory_mode:
        LOG.info("[purple]Factory run")
        for item in items:
            LOG.info(f"[purple]Item - {item}")
            with tempfile.TemporaryDirectory(
                prefix=f"{conf['project']}-", dir=args.temp_directory
            ) as tmp_dir:
                LOG.info('Using temporary directory "%s"', tmp_dir)
                main_temp_dir(tmp_dir, args, conf, version, item)
    else:
        with tempfile.TemporaryDirectory(
            prefix=f"{conf['project']}-", dir=args.temp_directory
        ) as tmp_dir:
            LOG.info('Using temporary directory "%s"', tmp_dir)
            main_temp_dir(tmp_dir, args, conf, version)


def main_temp_dir(tmp_dir, args, conf, conf_version, item=None):
    """
    Perform main bulk work work with *tmp_dir* set to a temporary directory which is deleted after
    the process has run.

    """
    # Form basic docker run command line
    docker_run_args = ["run", "--rm", "-i"]
    if not args.notty:
        docker_run_args.append("-t")

    # Using an image so mount working directory from current directory
    if conf["image"] != "":
        pwd = os.getcwd()
        permissions = "rw" if args.writable_workdir else "ro"
        docker_run_args.extend(["-v", f"{pwd}:{args.workdir}:{permissions}"])

    # Add terraform data volume
    if item:
        docker_run_args.extend(["-v", f'{conf["factory_prefix"]}_{item}:/terraform_data/'])
        docker_run_args.extend(["-e", f"FACTORY_ITEM={item}"])
        docker_run_args.extend(["-e", f'FACTORY_VARS_PATH={conf["factory_vars_path"]}'])
    else:
        docker_run_args.extend(["-v", f"{args.terraform_data_volume_name}:/terraform_data/"])

    # Decrypt secrets
    for sec in conf["secrets"]:
        # Parse source as a URL. Default scheme is "file"
        secret_components = urllib.parse.urlparse(sec["source"], scheme="file")

        # Store secret in a local temporary file
        _, tmp_secret = tempfile.mkstemp(prefix="secret-", dir=tmp_dir)

        # Local file secrets are decrypted using a key.
        if secret_components.scheme == "file":
            # Access secret as local file
            LOG.info('Decrypting local file secret "%s":', sec["name"])
            LOG.info('  "%s" -> "%s"...', sec["source"], tmp_secret)

            # Decrypt secret
            if not args.dry_run:
                kms_decrypt(
                    ciphertext_file=secret_components.path,
                    plaintext_file=tmp_secret,
                    key=sec["key"],
                )
        elif secret_components.scheme == "sm":
            # Access secret as secret manager URL
            LOG.info('Decrypting secret manager secret "%s":', sec["name"])
            LOG.info('  "%s" -> "%s"...', sec["source"], tmp_secret)

            # Parse the sm://... URL.
            project_id = secret_components.netloc
            secret_name = secret_components.path.lstrip("/")
            version = secret_components.fragment if secret_components.fragment != "" else "latest"

            # Access secret
            if not args.dry_run:
                secret_manager_access_secret(
                    project_id=project_id,
                    secret_name=secret_name,
                    version=version,
                    plaintext_file=tmp_secret,
                )
        else:
            raise RuntimeError(f"Unknown secret scheme: {secret_components.scheme}")

        # Add volume to docker command
        docker_run_args.extend(["-v", f"{tmp_secret}:{sec['target']}:ro"])

    # Set the terraform workspace.
    # NOTE: this "goes behind the back" of terraform to set the environment and is not a documented
    # way of achieving this. Short of having *another* wrapper within the container for terraform,
    # this is the cleanest way of ensuring the workspace.
    environment_path = os.path.join(tmp_dir, "environment")
    with open(environment_path, "w") as fobj:
        fobj.write(args.workspace)
    docker_run_args.extend(["-v", f"{environment_path}:/terraform_data/environment:ro"])

    # Add SSH agent forwarding
    docker_run_args.extend(ssh_agent_forward_args())

    # If we're in a version >= 1.1 config, look at the 'mount_docker_socket' configuration.
    if conf.get("mount_docker_socket") is not None:
        if conf_version < (1, 1):
            LOG.error("mount_docker_socket is only valid for configuration versions >= 1.1")
            sys.exit(1)
        if conf["mount_docker_socket"]:
            docker_run_args.extend(mount_docker_socket_args())

    # If we're in a version >= 1.2 config, look at the 'use_impersonation' configuration.
    if conf.get("use_impersonation") is not None:
        if conf_version < (1, 2):
            LOG.error("use_impersonation is only valid for configuration versions >= 1.2")
            sys.exit(1)
        if conf["use_impersonation"]:
            docker_run_args.extend(use_impersonation_args())

    # Add environment vars from configuration file
    for key, val in conf["env"].items():
        if val is None:
            LOG.info('Passing through env "%s" (="%s")', key, os.getenv(key, ""))
            docker_run_args.extend(["-e", key])
        else:
            LOG.info('Passing env "%s"="%s"', key, val)
            docker_run_args.extend(["-e", f"{key}={val}"])

    # Add any additional docker run arguments
    docker_run_args.extend(shlex.split(args.docker_run_args))

    # Add image name
    docker_run_args.append(args.image_name)

    # Split leading "--" from docker arguments if used on the command line to separate flags from
    # arguments to docker run.
    if args.cmdargs[:1] == ["--"]:
        args.cmdargs = args.cmdargs[1:]

    # Append given command arguments.
    docker_run_args.extend(args.cmdargs)

    # Run the docker run command logging the final command which was used.
    LOG.info("Executing: docker %s", " ".join([f'"{a}"' for a in docker_run_args]))
    exit_code = 0
    if not args.dry_run:
        exit_code = run_docker(*docker_run_args, allow_failure=True)

    if exit_code == 0 and item is not None:
        return
    sys.exit(exit_code)


def ssh_agent_forward_args():
    """
    Return docker run arguments suitable for forwarding the SSH agent socket.

    """
    system = platform.system()

    if system == "Linux":
        ssh_auth_sock = os.environ.get("SSH_AUTH_SOCK", "")
        if ssh_auth_sock == "":
            raise RuntimeError(
                "SSH_AUTH_SOCK environment variable is not set, is ssh-agent running?"
            )
    elif system == "Darwin":
        # This works with docker desktop version 3.3.1 and Engine: 20.10.5
        ssh_auth_sock = "/run/host-services/ssh-auth.sock"
    else:
        raise RuntimeError(f'Do not know how to forward SSH agent for platform "{system}"')

    return ["-v", f"{ssh_auth_sock}:/ssh-agent", "-e", "SSH_AUTH_SOCK=/ssh-agent"]


def mount_docker_socket_args():
    """
    Return docker run arguments suitable for forwarding the Docker socket.

    """
    if platform.system() not in ["Linux", "Darwin"]:
        raise RuntimeError(
            "mount_docker_socket is only currently supported on OS X and Linux hosts"
        )

    host_socket = "/var/run/docker.sock"  # TODO: is this portable?

    # Check that the socket exists and is a socker
    if not os.path.exists(host_socket):
        raise RuntimeError(f"Docker host socket {host_socket} does not exist.")
    if not stat.S_ISSOCK(os.stat(host_socket).st_mode):
        raise RuntimeError(f"Docker host socket {host_socket} is not a socket.")

    return ["-v", f"{host_socket}:/var/run/docker.sock"]


def use_impersonation_args():
    """
    Return docker run arguments suitable for forwarding application_default_credentials.json
    for service account impersonation.

    """
    if platform.system() not in ["Linux", "Darwin"]:
        raise RuntimeError("use_impersonation is only currently supported on OS X and Linux hosts")

    adc_json = os.path.join(
        os.path.expanduser("~"),
        ".config",
        "gcloud",
        "application_default_credentials.json",
    )

    if not os.path.exists(adc_json):
        raise RuntimeError(f"{adc_json} does not exist.")

    return ["-v", f"{adc_json}:/credentials.json"]


def create_parser():
    parser = argparse.ArgumentParser(description="Wrap docker run command")
    parser.add_argument(
        "-c",
        "--configuration",
        default=None,
        type=str,
        help="Location of configuration file",
    )
    parser.add_argument(
        "--dry-run",
        action="store_true",
        help="Do not actually decrypt secrets and run docker",
    )
    parser.add_argument("--quiet", action="store_true", help="Reduce logging verbosity")
    parser.add_argument("--debug", action="store_true", help="Increase logging verbosity")
    parser.add_argument(
        "--image-name",
        type=str,
        help="Name of Docker image to use/build, or "
        'override image to use/pull if "image" is '
        "specified in the configuration file",
        default="",
    )
    parser.add_argument(
        "--nobuild", action="store_true", help="Disable (re-)building of container"
    )
    parser.add_argument("--nopull", action="store_true", help="Disable (re-)pulling of image")
    parser.add_argument(
        "--workdir",
        default="/workdir",
        type=str,
        help="Override working directory inside the container (if an image is used)",
    )
    parser.add_argument(
        "--writable-workdir",
        action="store_true",
        help="Mount working directory as writable instead of read-only",
    )
    parser.add_argument("--workspace", type=str, default="development", help="Terraform workspace")
    parser.add_argument(
        "--terraform-data-volume-name",
        default="{project}-terraform-data",
        help="Name of Docker volume to hold terraform data directory.",
    )
    parser.add_argument(
        "--rm-terraform-data-volume",
        action="store_true",
        help="Delete terraform data volume before running docker run",
    )
    parser.add_argument(
        "-T",
        "--notty",
        action="store_true",
        help="Do not attach a pseudo TTY to the input",
    )
    parser.add_argument(
        "--docker-run-args",
        default="",
        help="Additional arguments to docker run. Split using shlex.split()",
    )
    parser.add_argument(
        "--temp-directory",
        default=None,
        type=str,
        help="Override location temporary directory is created in",
    )
    parser.add_argument(
        "-v", "--version", action="store_true", help="Print the version of this tool"
    )
    parser.add_argument(
        "-fo", "--factory-only", type=str, action="append", help="Only run listed factory items"
    )
    parser.add_argument(
        "-fe",
        "--factory-exclude",
        type=str,
        action="append",
        help="Run all Factory items except listed ones",
    )
    parser.add_argument(
        "cmdargs",
        nargs=argparse.REMAINDER,
        help="Remainder of arguments are passed to command run by docker run",
    )
    return parser


def kms_decrypt(*, ciphertext_file, plaintext_file, key):
    # Interpolate the default key config
    key_config = {}
    key_config.update(
        {
            "type": "google-kms",
            "project": "uis-automation-dm",
            "location": "global",
            "keyring": "terraform",
            "key": "admin-service-account",
        }
    )
    key_config.update(key)
    if key_config["type"] != "google-kms":
        raise RuntimeError(f'Unknown key type: {key_config["type"]}')

    client = kms.KeyManagementServiceClient()
    key_path = client.crypto_key_path(
        key_config["project"],
        key_config["location"],
        key_config["keyring"],
        key_config["key"],
    )

    with open(ciphertext_file, "rb") as in_fobj, open(plaintext_file, "wb") as out_fobj:
        out_fobj.write(
            client.decrypt(request={"name": key_path, "ciphertext": in_fobj.read()}).plaintext
        )


def secret_manager_access_secret(*, project_id, secret_name, version, plaintext_file):
    # Sanity check that there aren't any path components in the secret name or version.
    if "/" in secret_name or "/" in version:
        LOG.error("Secret URL must have form sm://PROJECT_ID/SECRET#VERSION")
        raise RuntimeError("bad secret URL")

    # Form the path to the secret
    client = secretmanager_v1.SecretManagerServiceClient()
    secret_path = client.secret_version_path(project_id, secret_name, version)

    # Access the secret using the Google API.
    with open(plaintext_file, "wb") as fobj:
        request = secretmanager_v1.AccessSecretVersionRequest(name=secret_path)
        fobj.write(client.access_secret_version(request=request).payload.data)


def run_docker(*args, allow_failure=False):
    """
    Run docker with the passed positional arguments. If allow_failure is True, do not check the
    return code.

    Returns the process return code from docker.

    """
    if DOCKER is None:
        raise RuntimeError('"docker" executable not found on system path')
    completed_process = subprocess.run(
        list(itertools.chain([DOCKER], args)), check=not allow_failure
    )
    return completed_process.returncode
