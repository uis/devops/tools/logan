"""
Synchronise secrets when bootstapping projects.

Usage:
    {cmd} (-h | --help)
    {cmd} sync [--secret NAME...] [--configuration FILE] [--verbose] [--dry-run]
    {cmd} destroy-old-versions [--secret NAME...] [--configuration FILE] [--verbose] [--dry-run]

Options:

    -h, --help                          Show a brief usage summary.
    -c FILE, --configuration FILE       Location of configuration file.
    --verbose                           Be verbose when logging.
    --dry-run                           Don't make changes but log what would be done.
                                        Always verbose.
    -s, NAME, --secret NAME             Work with provided secret(s) only.
"""

import logging
import os
import sys

import docopt
import structlog
from pydantic import ValidationError

from ..config import find_configuration
from . import Settings

LOG = structlog.get_logger()


def main():
    "Entry point for sanctuary tool."
    opts = docopt.docopt(__doc__.format(cmd=os.path.basename(sys.argv[0])))
    structlog.configure(
        processors=[
            structlog.contextvars.merge_contextvars,
            structlog.processors.add_log_level,
            structlog.processors.StackInfoRenderer(),
            structlog.dev.set_exc_info,
            structlog.dev.ConsoleRenderer(),
        ],
        wrapper_class=structlog.make_filtering_bound_logger(
            logging.INFO if opts["--verbose"] or opts["--dry-run"] else logging.WARN
        ),
        context_class=dict,
        logger_factory=structlog.PrintLoggerFactory(),
        cache_logger_on_first_use=False,
    )

    dry_run = opts["--dry-run"]
    if dry_run:
        LOG.info("Enabling dry run. Secrets will not be changed.")

    selected_secret_names = opts["--secret"]
    if selected_secret_names:
        LOG.info("Working with secret(s): %s", ", ".join(selected_secret_names))

    try:
        settings = Settings.from_config_file(find_configuration(opts["--configuration"]))
    except ValidationError as validation_error:
        LOG.error("Error validating configuration", error_message=str(validation_error))

    if opts["sync"]:
        settings.synchronise(dry_run=dry_run, selected_secret_names=selected_secret_names)
    elif opts["destroy-old-versions"]:
        settings.destroy_old_versions(dry_run=dry_run, selected_secret_names=selected_secret_names)
