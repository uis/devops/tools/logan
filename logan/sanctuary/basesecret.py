import typing

if typing.TYPE_CHECKING:
    # Trying to import Settings leads to circular imports so only do it when type-checking.
    from . import Settings  # pragma: nocover


class SecretError(RuntimeError):
    "Base for secret errors."


class SecretNotFoundError(SecretError):
    "The specified secret does not exist."


class UpdateNotSupportedError(SecretError):
    "The specified secret does not support updating its value."


class CreationNotSupportedError(UpdateNotSupportedError):
    # Creation can be seen as a special case of "update".
    "The specified secret does not support creation."


class BaseSecret:
    "Base class for secret providers."

    def fetch(self, settings: "Settings") -> bytes:
        """Fetch a secret. Raises SecretNotFoundError if it doesn't exist. Raises ValueError if
        the settings are incorrect in some way."""
        raise NotImplementedError()

    def ensure(self, settings: "Settings"):
        "Ensures that the secret exists and is correctly configured."
        raise NotImplementedError()

    def update(self, settings: "Settings", value: bytes):
        "Update a secret."
        raise NotImplementedError()

    def destroy_old_versions(self, settings: "Settings", *, dry_run: bool = False):
        "Destroy all previous versions of this secret leaving only the most recent one active."
        raise NotImplementedError()
