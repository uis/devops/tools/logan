import typing

import structlog
import yaml
from pydantic import BaseModel, Field, model_validator
from pydantic_settings import BaseSettings

from .basesecret import SecretError, SecretNotFoundError
from .google import GoogleSecret, GoogleSecretManagerSettings
from .onepasswordcli import OnePasswordCLIDocument, OnePasswordCLIItem

LOG = structlog.get_logger()


class SecretSpec(BaseModel):
    google_secret: typing.Optional[GoogleSecret] = Field(default=None, alias="google-secret")
    op_cli_item: typing.Optional[OnePasswordCLIItem] = Field(default=None, alias="op-cli-item")
    op_cli_document: typing.Optional[OnePasswordCLIDocument] = Field(
        default=None, alias="op-cli-document"
    )

    @model_validator(mode="after")
    def check_only_one_present(self) -> "SecretSpec":
        number_of_defined_fields = sum(
            1 if getattr(self, n, None) is not None else 0 for n in self.__pydantic_fields_set__
        )

        if number_of_defined_fields == 0:
            raise ValueError("At least one secret type must be defined")
        if number_of_defined_fields > 1:
            raise ValueError("At most one secret type must be defined")

        return self

    def fetch(self, settings: "Settings") -> bytes:
        "Fetch the secret value."
        for name in self.__pydantic_fields_set__:
            value = getattr(self, name)
            if value is not None:
                return value.fetch(settings)
        assert False, "Should not be reached"  # pragma: nocover

    def ensure(self, settings: "Settings"):
        "Ensure the secret exists and is up to date."
        for name in self.__pydantic_fields_set__:
            field_value = getattr(self, name)
            if field_value is not None:
                field_value.ensure(settings)

    def update(self, settings: "Settings", value: bytes):
        "Update the secret value."
        for name in self.__pydantic_fields_set__:
            field_value = getattr(self, name)
            if field_value is not None:
                field_value.update(settings, value)

    def destroy_old_versions(self, settings: "Settings", *, dry_run: bool = False):
        "Destroy any previous versions of this secret."
        for name in self.__pydantic_fields_set__:
            field_value = getattr(self, name)
            if field_value is not None:
                field_value.destroy_old_versions(settings, dry_run=dry_run)


class SecretSyncSpec(BaseModel):
    from_secret: SecretSpec = Field(alias="from")
    to_secret: SecretSpec = Field(alias="to")

    def synchronise(self, settings: "Settings", *, dry_run: bool):
        try:
            from_value = self.from_secret.fetch(settings)
        except ValueError as e:
            LOG.error("Error fetching 'from' secret", error=e, secret=self.from_secret)
            raise e

        # It is OK for the "to" secret to not exist yet,
        try:
            to_value = self.to_secret.fetch(settings)
        except SecretNotFoundError:
            to_value = None
        except ValueError as e:
            LOG.error("Error fetching 'to' secret", error=e, secret=self.to_secret)
            raise e

        LOG.info(
            "Ensuring that secret exists and is ready for update.",
            secret=self.to_secret,
        )
        self.to_secret.ensure(settings)

        if from_value != to_value:
            LOG.info(
                "Updating secret",
                from_secret=self.from_secret,
                to_secret=self.to_secret,
            )
            if not dry_run:
                self.to_secret.update(settings, from_value)

    def destroy_old_versions(self, settings: "Settings", *, dry_run: bool):
        self.to_secret.destroy_old_versions(settings, dry_run=dry_run)


class Settings(BaseSettings):
    google_secret: GoogleSecretManagerSettings = Field(
        default_factory=GoogleSecretManagerSettings, alias="google-secret"
    )

    secrets: dict[str, SecretSyncSpec] = Field(default_factory=dict)

    @classmethod
    def from_config_file(cls, config_path: str) -> "Settings":
        with open(config_path) as f:
            config_dict = yaml.safe_load(f).get("sanctuary", {})
            return cls.model_validate(config_dict)

    def synchronise(self, dry_run: bool = True, selected_secret_names: list = []):
        secret_names = selected_secret_names or list(self.secrets.keys())
        for secret_name in secret_names:
            secret_spec = self.secrets[secret_name]
            LOG.info("Processing secret", secret_name=secret_name)
            try:
                secret_spec.synchronise(self, dry_run=dry_run)
            except SecretError as e:
                LOG.error(
                    "Error processing secret.", secret_name=secret_name, error_message=str(e)
                )

    def destroy_old_versions(self, dry_run: bool, selected_secret_names: list = []):
        secret_names = selected_secret_names or list(self.secrets.keys())
        for secret_name in secret_names:
            secret_spec = self.secrets[secret_name]
            LOG.info("Destroying versions for secret", secret_name=secret_name)
            try:
                secret_spec.destroy_old_versions(self, dry_run=dry_run)
            except SecretError as e:
                LOG.error(
                    "Error processing secret.", secret_name=secret_name, error_message=str(e)
                )
