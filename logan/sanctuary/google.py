import functools
import typing

import structlog
from google.api_core.exceptions import NotFound
from google.auth import default as google_default_credentials
from google.auth.credentials import Credentials as GoogleCredentials
from google.auth.impersonated_credentials import (
    Credentials as GoogleImpersonatedCredentials,
)
from google.cloud import secretmanager_v1
from pydantic import BaseModel, EmailStr

from .basesecret import (
    BaseSecret,
    CreationNotSupportedError,
    SecretNotFoundError,
    UpdateNotSupportedError,
)

LOG = structlog.get_logger()

if typing.TYPE_CHECKING:
    # Trying to import Settings leads to circular imports so only do it when type-checking.
    from . import Settings  # pragma: no cover


class GoogleSecretManagerSettings(BaseModel):
    impersonate: typing.Optional[EmailStr] = None

    @functools.cached_property
    def credentials(self) -> GoogleCredentials:
        """
        Default Google credentials or credentials impersonating the selected service account if a
        service account has been selected.
        """

        default_credentials, _ = google_default_credentials()
        if self.impersonate is not None:
            LOG.info("Impersonating Google service account", service_account=self.impersonate)
            return GoogleImpersonatedCredentials(
                source_credentials=default_credentials,
                target_principal=self.impersonate,
                target_scopes=default_credentials.scopes,
            )
        LOG.info("Using application default Google credentials.")
        return default_credentials


class GoogleSecret(BaseModel, BaseSecret):
    # Project secret is located in.
    project: str

    # The name of the secret within the Google project.
    name: str

    # Optionally, the version of the secret to use. If omitted, the latest version is always used.
    version: str = "latest"

    # Optionally, whether to destroy previous versions of the secret when updating.
    destroy_previous_versions: bool = False

    def fetch(self, settings: "Settings") -> bytes:
        client = self._make_client(settings)
        request = secretmanager_v1.AccessSecretVersionRequest(
            name=self._secret_version_path(client)
        )
        try:
            response = client.access_secret_version(request=request)
        except NotFound as e:
            raise SecretNotFoundError(
                f"Secret '{self.name}' does not exist in project "
                f"'{self.project}' with version '{self.version}'"
            ) from e
        return response.payload.data

    def ensure(self, settings: "Settings"):
        if self.version != "latest":
            raise UpdateNotSupportedError("Cannot set a specific Google secret version.")

        # Check that the secret exists. Raises CreationNotSupportedError if it does not.
        client = self._make_client(settings)
        request = secretmanager_v1.GetSecretRequest(name=self._secret_path(client))

        try:
            client.get_secret(request=request)
        except NotFound as e:
            raise CreationNotSupportedError(
                "Google secrets must be created before being set."
            ) from e

    def update(self, settings: "Settings", value: bytes):
        if self.version != "latest":
            raise UpdateNotSupportedError("Cannot set a specific Google secret version.")

        client = self._make_client(settings)
        request = secretmanager_v1.AddSecretVersionRequest(
            parent=self._secret_path(client),
            payload=secretmanager_v1.SecretPayload(data=value),
        )

        try:
            client.add_secret_version(request=request)
        except NotFound as e:
            raise SecretNotFoundError() from e

        if self.destroy_previous_versions:
            self.destroy_old_versions(settings)

    def destroy_old_versions(self, settings: "Settings", *, dry_run=False):
        # Disable all but the most recent version of the secret.
        client = self._make_client(settings)
        LOG.info("Destroying previous versions of secret", name=self._secret_path(client))
        secret_versions = client.list_secret_versions(
            request=secretmanager_v1.ListSecretVersionsRequest(parent=self._secret_path(client))
        )
        for version in secret_versions.versions[1:]:
            # Non-destroyed versions should, according to the docs, not have the
            # destroy_time attribute present. We have observed non-destroyed secrets
            # do in fact have it set but with a sentinel value of "0". Accept both
            # the documented and observed behaviours when detecting destroyed secrets.
            destroy_time = getattr(version, "destroy_time", None)
            if destroy_time is None or destroy_time.ToSeconds() == 0:
                if dry_run:
                    LOG.info("Would destroy previous version", name=version.name)
                else:
                    LOG.info("Destroying previous version", name=version.name)
                    client.destroy_secret_version(
                        request=secretmanager_v1.DestroySecretVersionRequest(name=version.name)
                    )

    def _make_client(self, settings: "Settings") -> secretmanager_v1.SecretManagerServiceClient:
        return secretmanager_v1.SecretManagerServiceClient(
            credentials=settings.google_secret.credentials
        )

    def _secret_version_path(self, client: secretmanager_v1.SecretManagerServiceClient) -> str:
        return client.secret_version_path(self.project, self.name, self.version)

    def _secret_path(self, client: secretmanager_v1.SecretManagerServiceClient) -> str:
        return client.secret_path(self.project, self.name)
