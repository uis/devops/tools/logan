import json
import subprocess
import typing

import structlog
from pydantic import BaseModel, Field, model_validator

from .basesecret import BaseSecret, SecretNotFoundError, UpdateNotSupportedError

LOG = structlog.get_logger()

if typing.TYPE_CHECKING:
    # Trying to import Settings leads to circular imports so only do it when type-checking.
    from . import Settings  # pragma: nocover


class OnePasswordCLIItem(BaseModel, BaseSecret):
    item_id: str = Field(alias="item-id")
    fields: typing.Optional[list[str]] = None
    field: typing.Optional[str] = None
    use_field_labels: bool = False

    @model_validator(mode="after")
    def check_field_or_fields_present(self) -> "OnePasswordCLIItem":
        if self.fields is None and self.field is None:
            raise ValueError("At least one of 'field' or 'fields' must be defined")
        return self

    def fetch(self, settings: "Settings") -> bytes:
        process = subprocess.run(
            [
                "op",
                "item",
                "get",
                self.item_id,
                "--format",
                "json",
            ],
            capture_output=True,
            errors="utf8",
        )
        if process.returncode != 0:
            LOG.error(
                "Error calling 1password CLI",
                returncode=process.returncode,
                stderr=process.stderr,
            )
            raise SecretNotFoundError()
        item_data = json.loads(process.stdout)

        if self.fields is not None:
            secret_value = {}
            for field in item_data.get("fields", []):
                field_id = field["label"] if self.use_field_labels else field["id"]
                if field_id not in self.fields:
                    continue
                secret_value[field_id] = field["value"]
            return json.dumps(secret_value).encode("utf8")
        elif self.field is not None:
            for field in item_data.get("fields", []):
                field_id = field["label"] if self.use_field_labels else field["id"]
                if field_id == self.field:
                    return field["value"].encode("utf8")
            raise ValueError(f"Field does not exist in secret: {self.field!r}")
        else:
            # One of field or fields being defined is checked at model validation time.
            assert False, "Neither 'field' nor 'fields' is defined"  # pragma: nocover

    def ensure(self, settings: "Settings"):
        raise UpdateNotSupportedError("Updating 1password items is not yet implemented.")

    def update(self, settings: "Settings", value: bytes):
        raise UpdateNotSupportedError("Updating 1password items is not yet implemented.")


class OnePasswordCLIDocument(BaseModel, BaseSecret):
    document_id: str = Field(alias="document-id")

    def fetch(self, settings: "Settings") -> bytes:
        process = subprocess.run(
            [
                "op",
                "document",
                "get",
                self.document_id,
            ],
            capture_output=True,
        )
        if process.returncode != 0:
            LOG.error(
                "Error calling 1password CLI",
                returncode=process.returncode,
                stderr=process.stderr.decode("utf8"),
            )
            raise SecretNotFoundError()
        return process.stdout

    def ensure(self, settings: "Settings"):
        raise UpdateNotSupportedError("Updating 1password documents is not yet implemented.")

    def update(self, settings: "Settings", value: bytes):
        raise UpdateNotSupportedError("Updating 1password documents is not yet implemented.")
