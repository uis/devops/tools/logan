import typing

import pytest
import yaml


@pytest.fixture
def make_config(tmp_path):
    def make_config_implementation(config: dict[typing.Any, typing.Any]) -> str:
        config_path = tmp_path / "config.yaml"
        with open(config_path, "w") as f:
            yaml.dump(config, f)
        return str(config_path)

    return make_config_implementation


@pytest.fixture
def simple_sanctuary_config(faker, make_config) -> tuple[dict[typing.Any, typing.Any], str]:
    "A tuple of path, config for a simple test sync config."
    secret_name = faker.slug()
    config = {
        "sanctuary": {
            "secrets": {
                secret_name: {
                    "from": {
                        "google-secret": {
                            "project": faker.slug(),
                            "name": faker.slug(),
                        }
                    },
                    "to": {
                        "op-cli-item": {
                            "item-id": faker.slug(),
                            "fields": [faker.slug()],
                        },
                    },
                },
            }
        }
    }
    return config, secret_name


@pytest.fixture
def simple_sanctuary_config_dict(simple_sanctuary_config):
    return simple_sanctuary_config[0]


@pytest.fixture
def simple_sanctuary_config_secret_name(simple_sanctuary_config):
    return simple_sanctuary_config[1]


@pytest.fixture
def simple_sanctuary_config_path(simple_sanctuary_config_dict, make_config):
    return make_config(simple_sanctuary_config_dict)
