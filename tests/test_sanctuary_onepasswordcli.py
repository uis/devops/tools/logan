import json

import pytest
from pydantic import ValidationError
from testfixtures.popen import MockPopen

from logan.sanctuary import Settings
from logan.sanctuary.basesecret import SecretNotFoundError, UpdateNotSupportedError
from logan.sanctuary.onepasswordcli import OnePasswordCLIDocument, OnePasswordCLIItem


@pytest.fixture(autouse=True)
def mock_popen(mocker):
    "A MockPopen instance which mocks the subprocess.Popen interface."
    mock = MockPopen()
    mocker.patch("subprocess.Popen", mock)
    return mock


@pytest.fixture
def fake_item(faker):
    "A fake 1password item"
    return OnePasswordCLIItem.model_validate(
        {"item-id": faker.slug(), "fields": [faker.slug(), faker.slug()]}
    )


@pytest.fixture
def fake_single_field_item(faker):
    "A fake 1password item specifying a single raw field"
    return OnePasswordCLIItem.model_validate({"item-id": faker.slug(), "field": faker.slug()})


@pytest.fixture
def op_get_item_command(fake_item):
    "The op get command we expect for our fake item."
    return f"op item get {fake_item.item_id} --format json"


@pytest.fixture
def op_get_single_field_item_command(fake_single_field_item):
    "The op get command we expect for our fake single field item."
    return f"op item get {fake_single_field_item.item_id} --format json"


@pytest.fixture
def fake_document(faker):
    "A fake 1password document"
    return OnePasswordCLIDocument.model_validate({"document-id": faker.slug()})


@pytest.fixture
def op_get_document_command(fake_document):
    "The op get command we expect for our fake document."
    return f"op document get {fake_document.document_id}"


def test_field_or_fields_required(faker):
    "One of field or fields must be present."
    item_dict = {"item-id": faker.slug()}

    # With neither field nor fields, a validation error is raised.
    with pytest.raises(ValidationError):
        OnePasswordCLIItem.model_validate(item_dict)

    # Adding either field or fields should successfully validate
    OnePasswordCLIItem.model_validate({**item_dict, "field": faker.slug()})
    OnePasswordCLIItem.model_validate({**item_dict, "fields": [faker.slug()]})


def test_no_item_ensure(fake_item):
    "ensure() is not implemented"
    with pytest.raises(UpdateNotSupportedError):
        fake_item.ensure(Settings())


def test_no_item_update(fake_item, faker):
    "update() is not implemented"
    with pytest.raises(UpdateNotSupportedError):
        fake_item.update(Settings(), faker.slug().encode("ascii"))


def test_no_document_ensure(fake_document):
    "ensure() is not implemented"
    with pytest.raises(UpdateNotSupportedError):
        fake_document.ensure(Settings())


def test_no_document_update(fake_document, faker):
    "update() is not implemented"
    with pytest.raises(UpdateNotSupportedError):
        fake_document.update(Settings(), faker.slug().encode("ascii"))


def test_no_such_item(op_get_item_command, fake_item, mock_popen):
    "SecretNotFoundError is raised if the 1password command fails"
    mock_popen.set_command(op_get_item_command, returncode=1)
    with pytest.raises(SecretNotFoundError):
        fake_item.fetch(Settings())


def test_item_retrieved(op_get_item_command, fake_item, faker, mock_popen):
    "fetch() retrieves the value of the item"
    title = faker.sentence()
    fields = {f: faker.slug() for f in fake_item.fields}
    item = {
        "title": title,
        "fields": [{"id": k, "value": v} for k, v in fields.items()],
    }
    # Add an unrelated field to make sure we filter.
    item["fields"].append({"id": faker.slug(), "value": faker.slug()})
    expected_item = {k: v for k, v in fields.items()}

    mock_popen.set_command(op_get_item_command, stdout=json.dumps(item).encode("utf8"))
    assert json.loads(fake_item.fetch(Settings())) == expected_item


def test_item_retrieved_by_label(op_get_item_command, fake_item, faker, mock_popen):
    "fetch() retrieves the value of the item by label if so desired"
    title = faker.sentence()
    fake_item.use_field_labels = True
    fields = {f: faker.slug() for f in fake_item.fields}
    item = {
        "title": title,
        "fields": [{"id": faker.slug(), "label": k, "value": v} for k, v in fields.items()],
    }
    # Add an unrelated field to make sure we filter.
    item["fields"].append({"id": faker.slug(), "label": faker.slug(), "value": faker.slug()})
    expected_item = {k: v for k, v in fields.items()}

    mock_popen.set_command(op_get_item_command, stdout=json.dumps(item).encode("utf8"))
    assert json.loads(fake_item.fetch(Settings())) == expected_item


def test_bad_field_name(
    op_get_single_field_item_command, fake_single_field_item, faker, mock_popen
):
    "fetch() raises ValueError if only one field is specified and it does not exist in the secret"
    title = faker.sentence()
    item = {
        "title": title,
        "fields": [{"id": faker.slug(), "value": faker.slug()}],
    }
    mock_popen.set_command(
        op_get_single_field_item_command, stdout=json.dumps(item).encode("utf8")
    )
    with pytest.raises(ValueError):
        fake_single_field_item.fetch(Settings())


def test_single_field_item_retrieved(
    op_get_single_field_item_command, fake_single_field_item, faker, mock_popen
):
    "fetch() retrieves the value of field itself if only one field specified"
    title = faker.sentence()
    field_value = faker.slug()
    fields = {fake_single_field_item.field: field_value}
    item = {
        "title": title,
        "fields": [{"id": k, "value": v} for k, v in fields.items()],
    }
    # Add an unrelated field to make sure we filter.
    item["fields"].append({"id": faker.slug(), "value": faker.slug()})
    expected_value = field_value.encode("utf8")

    mock_popen.set_command(
        op_get_single_field_item_command, stdout=json.dumps(item).encode("utf8")
    )
    assert fake_single_field_item.fetch(Settings()) == expected_value


def test_single_field_item_retrieved_by_label(
    op_get_single_field_item_command, fake_single_field_item, faker, mock_popen
):
    "fetch() retrieves the value of field itself if only one field specified by label"
    title = faker.sentence()
    field_value = faker.slug()
    fake_single_field_item.use_field_labels = True
    fields = {fake_single_field_item.field: field_value}
    item = {
        "title": title,
        "fields": [{"label": k, "id": faker.slug(), "value": v} for k, v in fields.items()],
    }
    # Add an unrelated field to make sure we filter.
    item["fields"].append({"label": faker.slug(), "id": faker.slug(), "value": faker.slug()})
    expected_value = field_value.encode("utf8")

    mock_popen.set_command(
        op_get_single_field_item_command, stdout=json.dumps(item).encode("utf8")
    )
    assert fake_single_field_item.fetch(Settings()) == expected_value


def test_no_such_document(op_get_document_command, fake_document, mock_popen):
    "SecretNotFoundError is raised if the 1password command fails"
    mock_popen.set_command(op_get_document_command, returncode=1)
    with pytest.raises(SecretNotFoundError):
        fake_document.fetch(Settings())


def test_document_retrieved(op_get_document_command, fake_document, faker, mock_popen):
    "fetch() retrieves the value of the document"
    expected_value = faker.slug().encode("utf8")
    mock_popen.set_command(op_get_document_command, stdout=expected_value)
    assert fake_document.fetch(Settings()) == expected_value
