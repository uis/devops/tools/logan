import pytest

from logan.sanctuary import Settings


@pytest.fixture
def settings(simple_sanctuary_config_path):
    return Settings.from_config_file(simple_sanctuary_config_path)


@pytest.fixture(autouse=True)
def mock_secret_methods(settings, mocker, faker):
    "Mock all secrets in the settings."
    for sync_spec in settings.secrets.values():
        for secret_spec in (sync_spec.from_secret, sync_spec.to_secret):
            for field_name in secret_spec.__pydantic_fields_set__:
                secret = getattr(secret_spec, field_name)
                if secret is None:
                    continue
                mock = mocker.MagicMock()
                mock.fetch.return_value = faker.slug().encode("utf8")
                setattr(secret_spec, field_name, mock)


def test_simple_sync(settings):
    settings.synchronise(dry_run=False)
    for sync_spec in settings.secrets.values():
        for field_name in sync_spec.from_secret.__pydantic_fields_set__:
            secret = getattr(sync_spec.from_secret, field_name)
            if secret is None:
                continue
            secret.fetch.assert_called()
            secret.update.assert_not_called()
            secret.ensure.assert_not_called()
        for field_name in sync_spec.to_secret.__pydantic_fields_set__:
            secret = getattr(sync_spec.to_secret, field_name)
            if secret is None:
                continue
            secret.fetch.assert_called()
            secret.update.assert_called()
            secret.ensure.assert_called()


def test_simple_sync_selected(settings):
    settings.synchronise(dry_run=False, selected_secret_names=list(settings.secrets.keys()))
    for sync_spec in settings.secrets.values():
        for field_name in sync_spec.from_secret.__pydantic_fields_set__:
            secret = getattr(sync_spec.from_secret, field_name)
            if secret is None:
                continue
            secret.fetch.assert_called()
            secret.update.assert_not_called()
            secret.ensure.assert_not_called()
        for field_name in sync_spec.to_secret.__pydantic_fields_set__:
            secret = getattr(sync_spec.to_secret, field_name)
            if secret is None:
                continue
            secret.fetch.assert_called()
            secret.update.assert_called()
            secret.ensure.assert_called()


def test_dry_run(settings):
    "In dry-run mode, we should have fetched all secrets but not updated them."
    settings.synchronise(dry_run=True)
    for sync_spec in settings.secrets.values():
        for secret_spec in (sync_spec.from_secret, sync_spec.to_secret):
            for field_name in secret_spec.__pydantic_fields_set__:
                secret = getattr(secret_spec, field_name)
                if secret is None:
                    continue
                secret.fetch.assert_called()
                secret.update.assert_not_called()
