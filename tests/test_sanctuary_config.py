import pytest
from pydantic import ValidationError

from logan.sanctuary import Settings


def test_config_can_have_empty_sanctuary_section(make_config):
    # Will raise if there is a problem.
    Settings.from_config_file(make_config({"sanctuary": {}}))


def test_config_can_have_missing_sanctuary_section(make_config):
    s = Settings.from_config_file(make_config({}))
    assert s.google_secret is not None


def test_config_can_have_google_secret_config(make_config):
    s = Settings.from_config_file(make_config({"sanctuary": {"google-secret": {}}}))
    assert s.google_secret is not None


def test_config_can_have_google_secret_config_with_impersonation(make_config, faker):
    email = faker.email()
    s = Settings.from_config_file(
        make_config({"sanctuary": {"google-secret": {"impersonate": email}}})
    )
    assert s.google_secret.impersonate == email


def test_google_service_account_must_be_email(make_config, faker):
    with pytest.raises(ValidationError):
        Settings.from_config_file(
            make_config({"sanctuary": {"google-secret": {"impersonate": faker.slug()}}})
        )


def test_basic_sync_config(simple_sanctuary_config_path):
    Settings.from_config_file(simple_sanctuary_config_path)


@pytest.mark.parametrize("whence", ["from", "to"])
def test_at_least_one_secret_source_required(
    whence,
    make_config,
    simple_sanctuary_config_dict,
    simple_sanctuary_config_secret_name,
):
    simple_sanctuary_config_dict["sanctuary"]["secrets"][simple_sanctuary_config_secret_name][
        whence
    ] = {}
    with pytest.raises(ValidationError):
        Settings.from_config_file(make_config(simple_sanctuary_config_dict))


@pytest.mark.parametrize("whence", ["from", "to"])
def test_at_most_one_secret_source_required(
    whence,
    make_config,
    simple_sanctuary_config_dict,
    simple_sanctuary_config_secret_name,
    faker,
):
    simple_sanctuary_config_dict["sanctuary"]["secrets"][simple_sanctuary_config_secret_name][
        whence
    ] = {
        "google-secret": {"project": faker.slug(), "name": faker.slug()},
        "op-cli-item": {"item-id": faker.slug(), "field": faker.slug()},
    }
    with pytest.raises(ValidationError):
        Settings.from_config_file(make_config(simple_sanctuary_config_dict))
