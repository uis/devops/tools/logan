import pytest
from google.api_core.exceptions import NotFound
from google.auth.credentials import Credentials
from google.cloud import secretmanager_v1

from logan.sanctuary import Settings
from logan.sanctuary.basesecret import (
    CreationNotSupportedError,
    SecretNotFoundError,
    UpdateNotSupportedError,
)
from logan.sanctuary.google import GoogleSecret, GoogleSecretManagerSettings


class MockCredentials(Credentials):
    """
    Mock credentials object compatible with that used by Google clients.
    """

    @property
    def scopes(self):
        return []

    def refresh(self, request):
        pass


@pytest.fixture(autouse=True)
def mock_default_credentials(mocker, faker):
    "Mock credentials returned from google.auth.default()"
    mock = mocker.patch("logan.sanctuary.google.google_default_credentials")
    credentials = MockCredentials()
    mock.return_value = credentials, faker.slug()
    return credentials


@pytest.fixture(autouse=True)
def mock_impersonated_credentials(mocker):
    "Mock impersonated credentials constructor."
    mock = mocker.patch("logan.sanctuary.google.GoogleImpersonatedCredentials")
    mock.return_value = MockCredentials()
    return mock


@pytest.fixture(autouse=True)
def mock_secret_manager_client(mocker):
    "Mock secret manager client"
    real_client = secretmanager_v1.SecretManagerServiceClient(credentials=MockCredentials())
    mock = mocker.patch("logan.sanctuary.google.secretmanager_v1.SecretManagerServiceClient")
    client = mock.return_value
    client.secret_version_path.side_effect = real_client.secret_version_path
    client.secret_path.side_effect = real_client.secret_path
    return client


@pytest.fixture
def google_secret(faker) -> GoogleSecret:
    "A fake Google secret spec"
    return GoogleSecret(project=faker.slug(), name=faker.slug())


def test_default_credentials(mock_default_credentials):
    "Without impersonation configured, we should use Google application default credentials"
    settings = GoogleSecretManagerSettings()
    assert settings.credentials is mock_default_credentials


def test_impersonated_credentials(faker, mock_impersonated_credentials, mock_default_credentials):
    "With impersonation configured, we should use impersonated credentials."
    service_account_email = faker.email()
    settings = GoogleSecretManagerSettings(impersonate=service_account_email)
    assert settings.credentials is mock_impersonated_credentials.return_value
    mock_impersonated_credentials.assert_called_once_with(
        source_credentials=mock_default_credentials,
        target_principal=service_account_email,
        target_scopes=mock_default_credentials.scopes,
    )


def test_fetch(google_secret, mock_secret_manager_client):
    google_secret.fetch(Settings())
    mock_secret_manager_client.access_secret_version.assert_called_once()


def test_fetch_not_found(google_secret, mock_secret_manager_client):
    "If the secret does not exist, fetch() returns NotFound."
    mock_secret_manager_client.access_secret_version.side_effect = NotFound("not found")
    with pytest.raises(SecretNotFoundError):
        google_secret.fetch(Settings())


def test_ensure(google_secret, mock_secret_manager_client):
    "Ensure checks that the secret exists and doesn't create if not necessary."
    google_secret.ensure(Settings())
    mock_secret_manager_client.get_secret.assert_called_once()


def test_ensure_no_version(google_secret):
    "One cannot ensure (i.e. create) a specific version of a secret."
    google_secret.version = "1"
    with pytest.raises(UpdateNotSupportedError):
        google_secret.ensure(Settings())


def test_ensure_cannot_create(google_secret, mock_secret_manager_client):
    "If the secret does not exist, ensure() complains."
    mock_secret_manager_client.get_secret.side_effect = NotFound("not found")
    with pytest.raises(CreationNotSupportedError):
        google_secret.ensure(Settings())


def test_update(faker, google_secret, mock_secret_manager_client):
    value = faker.slug().encode("utf8")
    google_secret.update(Settings(), value)
    mock_secret_manager_client.add_secret_version.assert_called_once_with(
        request=secretmanager_v1.AddSecretVersionRequest(
            parent=mock_secret_manager_client.secret_path(
                google_secret.project, google_secret.name
            ),
            payload=secretmanager_v1.SecretPayload(data=value),
        ),
    )


def test_update_non_existant_secret(faker, google_secret, mock_secret_manager_client):
    "If the secret does not exist, update() raises."
    value = faker.slug().encode("utf8")
    mock_secret_manager_client.add_secret_version.side_effect = NotFound("not found")
    with pytest.raises(SecretNotFoundError):
        google_secret.update(Settings(), value)


def test_update_secret_version(faker, google_secret, mock_secret_manager_client):
    "Cannot update a specific version of a secret."
    value = faker.slug().encode("utf8")
    google_secret.version = "1"
    with pytest.raises(UpdateNotSupportedError):
        google_secret.update(Settings(), value)
