import pytest

from logan.sanctuary import Settings
from logan.sanctuary.basesecret import BaseSecret


def test_fetch_not_implemented():
    with pytest.raises(NotImplementedError):
        BaseSecret().fetch(Settings())


def test_ensure_not_implemented():
    with pytest.raises(NotImplementedError):
        BaseSecret().ensure(Settings())


def test_update_not_implemented(faker):
    with pytest.raises(NotImplementedError):
        BaseSecret().update(Settings(), faker.slug().encode("utf8"))
